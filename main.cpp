#include <iostream>
#include <filesystem>
#include <string>
#include <vector>
#include <fstream>
#include <set>

using namespace std;

// Constants
const int SUCCESS = 0;
const int PARAMETER_NUMBER_ERROR = 1;
const char* PARAMETER_NUMBER_ERROR_MESSAGE = "Wrong number of parameters, should be pm command arg";
const int PARAMETER_VALUE_ERROR = 2;
const char* PARAMETER_VALUE_ERROR_MESSAGE = "No such command.";
const int FILE_ALREADY_EXISTS_ERROR = 3;
const char* FILE_ALREADY_EXISTS_ERROR_MESSAGE = "A folder of that name already exists. Aborting.";
const char* CREATE_PROJECT = "create_project";


//binding local repo with GitLab repo
int git_init(const string &project_name, const string &remote_url) {
    cout<< filesystem::current_path() << endl;
    cout << "Initializing local repo" << endl;
    system(("cd "+project_name+"&& git init &&git remote add origin " +\
    remote_url+" && touch README.md && touch LICENSE && touch .gitignore").c_str());
    filesystem::create_directory(project_name+"/src");
    filesystem::create_directory(project_name+"/tests");
    filesystem::create_directory(project_name+"/docs");
    filesystem::create_directory(project_name+"/bin");
    filesystem::create_directory(project_name+"/lib");
    filesystem::create_directory(project_name+"/features");
    system(("cd "+project_name+"&& ls -al").c_str());
    //create basic files

    //bind local repo with GitLab repo
    system(("cd "+project_name+"&& git branch -M main ").c_str());
    cout << "Project pushed to GitLab successfully." << endl;
    return SUCCESS;
}

// Function for creating a git project.
int create_project(const string &project_name, const string &project_path, const vector<string> &current_files) {
    string illegal_characters = "\\/:*?\"<>|";
    // check if project already exists
    for (const auto & folder : current_files)
    {
        if (folder == project_name)
        {
            cerr << FILE_ALREADY_EXISTS_ERROR_MESSAGE<< endl;
            return FILE_ALREADY_EXISTS_ERROR;
        }
        else if (project_name.find_first_of(illegal_characters) != string::npos)
        {
            cerr << "Bad characters in folder name." << endl;
            return FILE_ALREADY_EXISTS_ERROR;
        }

    }
    // create project and output
    cout <<"Initialized empty Git repository in"+ project_path + "/" + project_name + "/.git/" << endl;
    filesystem::create_directory(project_name);
    //initialize git repo
    git_init(project_name, "https://csgitlab.reading.ac.uk/rb004046/coursework2.git");
    int result = system(("cd "+project_name).c_str());

    return result;
}




// add features in git project. This piece of code is working, but is bad...
int add_feature(const string &current_path, const string &feature_name) {
    // check if project exists
    filesystem::current_path(current_path+"/features");
    if (filesystem::exists(feature_name))
    {
        cerr << "A folder of that name already exists.  Aborting." << endl;
        return FILE_ALREADY_EXISTS_ERROR;
    }

    filesystem::create_directory(current_path+"/features/"+feature_name);
    // create feature and output
    cout << "Created feature " + feature_name + " in project " + current_path << endl;
    filesystem::create_directory(current_path+"/features/"+feature_name+"/src");
    filesystem::create_directory(current_path+"/features/"+feature_name+"/tests");
    filesystem::create_directory(current_path+"/features/"+feature_name+"/docs");
    filesystem::create_directory(current_path+"/features/"+feature_name+"/bin");
    filesystem::create_directory(current_path+"/features/"+feature_name+"/lib");

    //show the files in new feature
    system( "ls -al");
    return SUCCESS;
}

// Function for adding tags to a git project.
int add_tag(const string &current_path, const string &tag_name) {
    // check if project exists
    if (!filesystem::exists(".pm_tag"))
    {
        cerr << "A folder of that name already exists.  Aborting." << endl;
        return FILE_ALREADY_EXISTS_ERROR;
    }

    // create tag file in project
    ofstream tag_file;
    tag_file.open(current_path+"/.pm_tag");
    tag_file << tag_name;
    tag_file.close();

    //show the files in new tag
    system( "ls -al");
    return SUCCESS;
}

int main(int argc, char *argv[])
{
    // Check number of parameters
    if (argc != 3)
    {
        cerr << PARAMETER_NUMBER_ERROR_MESSAGE << endl;
        return PARAMETER_NUMBER_ERROR;
    }
    const string tool_name = argv[1];
    const string arg = argv[2];

    // get path to the project
    string path = filesystem::current_path();
    // get all folders in the path
    vector<string> folders;
    for (const auto & entry : filesystem::directory_iterator(path))
    {
        // change path to string
        string path_string = entry.path().string();
        // get last folder name
        string folder_name = path_string.substr(path_string.find_last_of("/") + 1);
        folders.insert(folders.end(), folder_name);
    }

    // create project and create basic file structure
    if(tool_name == CREATE_PROJECT)
    {
        cout <<"Initialized empty Git repository in"+ path + "/" + arg + "/.git/" << endl;
        //initialize git repo
        create_project(arg, path, folders);
    }
    else if (tool_name == "add_feature")
    {
        string current_project = path.substr(path.find_last_of("/") + 1);
        return add_feature(path, arg);
    }
    else if (tool_name == "add_tag")
    {
        return add_tag(path, arg);
    }
    else if (tool_name == "output_svg"){
        //get all subfolders in the current path
        int depth = 1;
        std::set<std::string> subfolders;
        filesystem::current_path(path+"/"+arg);
        //recursively get all subfolders with depth
        for (const auto & entry : filesystem::recursive_directory_iterator(path+"/"+arg))
        {
            // change path to string
            string path_string = entry.path().string();
            // get last folder name
            string folder_name = path_string.substr(path_string.find_last_of("/") + 1);
            subfolders.insert(folder_name);
        }
        //print the subfolders
        for (const auto & subfolder : subfolders)
            std::cout << subfolder << '\n';
        //create the svg file
        ofstream svg_file;
        svg_file.open(path+"/"+arg+"/project.svg");
        svg_file << "<svg xmlns=\"http://www.w3.org/2000/svg\" width=\"100\" height=\"100\">\n";
    }
    else
    {
        cerr << PARAMETER_NUMBER_ERROR_MESSAGE << endl;
        return PARAMETER_VALUE_ERROR;
    }

    return SUCCESS;

}


