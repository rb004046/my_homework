# Project Manager tool

## Tools

### 1. create_project

Usage: `pm create_project <project_name>`

Description: Create a new project with the given name.
             Create the basic structure of the project.


### 2. bind_repo

Usage: Auto bind the project to the remote repository when we create the project.

Description: Bind a remote repository to the current project.
             The remote repository will be used to push the project.

**Note**: The remote repository must be empty. If it is not empty, the bind will fail.

### 3. add_feature

Usage: `pm add_feature <feature_name>`

Description: Add a new feature to the current project.
             The feature will be created in the `features` directory.

### 4. add_tag

Usage: `pm add_tag <tag_name>`

Description: Add a new tag to the current project.
             The tag will be created in the `tags` directory.

